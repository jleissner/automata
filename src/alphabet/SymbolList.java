package alphabet;

import java.util.ArrayList;

/**
 * Created by jleis on 04.05.2017.
 */
public class SymbolList extends ArrayList<Symbol> {

    public boolean contains(String symbol) {
        for (Symbol s : this) {
            if (s.getRepresentation().equals(symbol)) {
                return true;
            }
        }
        return false;
    }

    public boolean contains(Symbol symbol) {
        return contains(symbol.getRepresentation());
    }
}

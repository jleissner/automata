package alphabet;

/**
 * Created by jleis on 04.05.2017.
 */
public class Symbol {
    private String representation;

    public Symbol(String representation) {
        this.representation = representation;
    }

    public String getRepresentation() {
        return representation;
    }

    public boolean isEpsilonSymbol() {
        return equals(new Epsilon());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass().toString().equals(getClass().toString()) || obj.getClass().toString().equals(Epsilon.class.toString())) {
            Symbol s = (Symbol) obj;
            return s.getRepresentation().equals(getRepresentation());
        }
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return representation;
    }
}

package alphabet;

/**
 * Created by jleis on 04.05.2017.
 */
public class Epsilon extends Symbol {

    public Epsilon() {
        super("\u03B5");
    }

    @Override
    public boolean isEpsilonSymbol() {
        return true;
    }
}

package alphabet;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jan-Frederik Leißner on 04.05.2017.
 */
public class Alphabet {
    private SymbolList symbols;

    public Alphabet(SymbolList symbols) {
        setSymbols(symbols);
    }

    public Alphabet() {
        this(new SymbolList());
    }

    public void addSymbol(Symbol symbol) {
        if (!symbols.contains(symbol)) {
            symbols.add(symbol);
        }
    }

    public void addSymbol(String symbol_representation) {
        addSymbol(new Symbol(symbol_representation));
    }

    public void addEpsilon() {
        addSymbol(new Epsilon());
    }

    private void setSymbols(SymbolList symbols) {
        this.symbols = symbols;
    }

    public SymbolList getSymbols() {
        return symbols;
    }

    @Override
    public String toString() {
        List<String> result = new ArrayList<>();
        for (Symbol s : symbols) {
            result.add(s.toString());
        }
        return "{" + String.join(";", result) + "}";
    }

    public void printToConsole() {
        System.out.println("ALPHABET:");
        System.out.println(this);
    }
}

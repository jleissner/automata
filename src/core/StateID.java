package core;

/**
 * Created by jleis on 04.05.2017.
 */
public class StateID {
    private String id;

    public StateID(String id) {
        this.id = id;
    }

    public String getID() {
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass().toString().equals(getClass().toString())) {
            StateID i = (StateID) obj;
            return i.getID().equals(getID());
        }
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return id;
    }

    @Override
    public int hashCode() {
        return getID().hashCode();
    }
}

package core;

/**
 * Created by jleis on 04.05.2017.
 */
public class State {
    private StateID id;

    public State(StateID id) {
        this.id = id;
    }

    public StateID getID() {
        return id;
    }

    @Override
    public String toString() {
        return id.toString();
    }
}

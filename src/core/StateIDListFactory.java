package core;

/**
 * Created by jleis on 04.05.2017.
 */
public class StateIDListFactory {

    public static StateIDList createFromString(String states_to_generate) {
        return createFromString(states_to_generate, " ");
    }

    public static StateIDList createFromString(String states_to_generate, String delimiter) {
        StateIDList list = new StateIDList();
        for (String state : states_to_generate.split(delimiter)) {
            String trimmed = state.trim();
            if (trimmed.length() > 0) {
                list.add(new StateID(trimmed));
            }
        }
        return list;
    }
}

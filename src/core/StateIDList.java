package core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Created by jleis on 04.05.2017.
 */
public class StateIDList extends ArrayList<StateID> {
    public StateIDList(Set<StateID> remove_duplicates) {
        super(remove_duplicates);
    }

    public StateIDList(){
        super();
    }

    @Override
    public String toString() {
        List<String> result = new ArrayList<>();
        for (StateID s : this) {
            result.add(s.toString());
        }
        return "{" + String.join(";", result) + "}";
    }
}

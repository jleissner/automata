package main;

import alphabet.Alphabet;
import alphabet.Epsilon;
import automat.Automat;
import automat.TransferFunctionList;
import automat.TransferFunctionListFactory;
import core.StateIDList;
import core.StateIDListFactory;
import grammar.Grammar;
import grammar.GrammarStateList;
import grammar.GrammarStateListFactory;

/**
 * Created by jleis on 03.05.2017.
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("Moin moin!");

        System.out.println("---------------------------------------------");

        Alphabet alphabet = new Alphabet();
        alphabet.addEpsilon();
        alphabet.addSymbol("0");
        alphabet.addSymbol("1");
        System.out.println("ALPHABET:\n" + alphabet);

        System.out.println("---------------------------------------------");

        GrammarStateList states = GrammarStateListFactory.createFromString(alphabet, "S -> 0 S | 1 A | 1; A -> 0 B | 1 A | 0 | 1; B -> 0 S | 1 A | 1");
        Grammar grammar = new Grammar(states);
        System.out.println("GRAMMATIK:\n" + grammar);
        System.out.println("isRegular: " + grammar.isRegular());

        System.out.println("---------------------------------------------");

        StateIDList automat_states = StateIDListFactory.createFromString("S A B", " ");
        StateIDList final_states = StateIDListFactory.createFromString("A B", " ");

        TransferFunctionList transfer_functions_dea = TransferFunctionListFactory.createFromString("S 0/a S ; S 1/b A ; A 0 B ; A 1/d A ; B 0/e S ; B 1 A");
        TransferFunctionList transfer_functions_nea = TransferFunctionListFactory.createFromString("S ε/x B ; S 0/a S ; S 1/b A ; A ε S ; A 0 B A ; A 1/d A ; B 0/e S ; B 1 A B");

        Automat dea = new Automat(automat_states, alphabet, transfer_functions_dea, final_states);
        System.out.println("AUTOMAT_DEA:\n" + dea);
        System.out.println("isDeterministic: " + dea.isDeterministic());

        System.out.println("---------------------------------------------");

        Automat nea = new Automat(automat_states, alphabet, transfer_functions_nea, final_states);
        System.out.println("AUTOMAT_NEA:\n" + nea);
        System.out.println("isDeterministic: " + nea.isDeterministic());
        System.out.println("----REMOVED EPSILON TRANSFERS----");
        nea.removeEpsilonTransfers();
        System.out.println("AUTOMAT_NEA:\n" + nea);
    }
}

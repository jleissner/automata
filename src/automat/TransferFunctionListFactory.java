package automat;

import alphabet.Epsilon;
import alphabet.Symbol;
import core.StateID;
import core.StateIDList;
import core.StateIDListFactory;

import java.util.Arrays;

/**
 * Created by jleis on 04.05.2017.
 */
public class TransferFunctionListFactory {
    public static TransferFunctionList createFromString(String s) {
        TransferFunctionList functions = new TransferFunctionList();
        String[] single_functions = s.split(";");
        for (String single_function : single_functions) {
            String[] parts = single_function.trim().split(" ");
            /* SOURCE_ID */
            StateID source = new StateID(parts[0]);
            String[] symbol_with_output = parts[1].split("/");
            /* SYMBOL */
            Symbol symbol = new Symbol(symbol_with_output[0]);
            /* OUTPUT */
            Symbol output = new Epsilon();
            if (symbol_with_output.length == 2) {
                output = new Symbol(symbol_with_output[1]);
            }
            String[] target_ids = Arrays.copyOfRange(parts, 2, parts.length);
            /* TARGETS */
            StateIDList targets = StateIDListFactory.createFromString(String.join(" ", target_ids));
            functions.add(new TransferFunction(source, symbol, targets, output));
        }
        return functions;
    }
}

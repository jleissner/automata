package automat;

import alphabet.Epsilon;
import alphabet.Symbol;
import core.State;
import core.StateID;

/**
 * Created by jleis on 04.05.2017.
 */
public class AutomatState extends State {

    private Symbol output;

    public AutomatState(StateID id, Symbol output) {
        super(id);
        this.output = output;
    }

    public AutomatState(StateID id) {
        this(id, new Epsilon());
    }

    public Symbol getOutput() {
        return output;
    }

}

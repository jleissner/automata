package automat;

import alphabet.Epsilon;
import alphabet.Symbol;
import core.StateID;
import core.StateIDList;

import java.util.*;

/**
 * Created by jleis on 04.05.2017.
 */
public class TransferFunction {
    private StateID source;
    private Symbol symbol;
    private StateIDList targets;
    private Symbol output;

    public TransferFunction(StateID source, Symbol symbol, StateIDList targets, Symbol output) {
        this.source = source;
        this.symbol = symbol;
        this.targets = targets;
        this.output = output;
    }

    public TransferFunction(StateID source, Symbol symbol, StateIDList targets) {
        this(source, symbol, targets, new Epsilon());
    }

    public TransferFunction(StateID source, Symbol symbol) {
        this(source, symbol, new StateIDList(), new Epsilon());
    }

    public void setOutput(Symbol output) {
        this.output = output;
    }

    public Symbol getOutput() {
        return output;
    }

    public StateID getSource() {
        return source;
    }

    public Symbol getSymbol() {
        return symbol;
    }

    public StateIDList getTargets() {
        return targets;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass().toString().equals(getClass().toString())) {
            TransferFunction f = (TransferFunction) obj;
            return f.getSource().equals(getSource()) && f.getSymbol().equals(getSymbol()) && f.getTargets().equals(getTargets());
        }
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return "\u03B4(" + source + "," + symbol + ")=[" + output + "]" + targets;
    }

    public static TransferFunction add(TransferFunction a, TransferFunction b){
        List<String> new_name = new ArrayList<>();
        new_name.add(a.getSource().toString());
        new_name.add(b.getSource().toString());
        Collections.sort(new_name);
        return TransferFunction.add(a,b,String.join("",new_name));
    }

    public static TransferFunction add(TransferFunction a, TransferFunction b, String new_name){
        if(new_name == null){
            return add(a,b);
        }
        StateIDList targets = a.getTargets();
        targets.addAll(b.getTargets());
        Set<StateID> remove_duplicates = new HashSet<StateID>();
        remove_duplicates.addAll(targets);
        targets = new StateIDList(remove_duplicates);
        TransferFunction f = new TransferFunction(new StateID(new_name),a.getSymbol(),targets,a.getOutput());
        return f;
    }

    public static TransferFunction add(TransferFunctionList list, String new_name){
        TransferFunction result = list.get(0);
        for(int i = 1; i<list.size(); i++){
            result = TransferFunction.add(result,list.get(i),new_name);
        }
        return result;
    }

    public static TransferFunction add(TransferFunctionList list){
        return add(list,null);
    }
}

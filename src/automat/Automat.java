package automat;

import alphabet.Alphabet;
import alphabet.Epsilon;
import alphabet.Symbol;
import core.StateID;
import core.StateIDList;

/**
 * Created by jleis on 04.05.2017.
 */
public class Automat {
    private StateIDList states;
    private Alphabet alphabet;
    private TransferFunctionList functions;
    private StateID initial_state;
    private StateIDList final_states;

    public Automat(StateIDList states, Alphabet alphabet, TransferFunctionList functions, StateID initial_state, StateIDList final_states) {
        this.states = states;
        this.alphabet = alphabet;
        this.functions = functions;
        this.initial_state = initial_state;
        this.final_states = final_states;
    }

    public Automat(StateIDList states, Alphabet alphabet, TransferFunctionList functions, StateIDList final_states) {
        this(states, alphabet, functions, states.get(0), final_states);
    }

    public boolean isDeterministic() {
        for (TransferFunction function : functions) {
            if (function.getTargets().size() > 1 || function.getSymbol().equals(new Epsilon())) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        String result = "";
        result += "STATES: " + states + "\n";
        result += "ALPHABET: " + alphabet + "\n";
        result += "TRANSFER_FUNCTIONS: " + functions + "\n";
        result += "INITIAL_STATE: " + initial_state + "\n";
        result += "FINAL_STATES: " + final_states;
        return result;
    }

    public void removeEpsilonTransfers() {
        TransferFunctionList toRemove = functions.findEpsilonTransfers();
        TransferFunctionList newGenerated = new TransferFunctionList();
        TransferFunctionList noMoreNeeded = new TransferFunctionList();
        noMoreNeeded.addAll(toRemove);
        for (TransferFunction f : toRemove) {
            TransferFunctionList toChange = functions.findTransfersWithSource(f.getSource());
            noMoreNeeded.addAll(toChange);
            TransferFunctionList toAdd = functions.findTransfersWithSources(f.getTargets());
            for(Symbol s : alphabet.getSymbols()){
                if(s.isEpsilonSymbol()){
                    continue;
                }
                TransferFunctionList base = toChange.findTransfersWithSymbol(s);
                base.addAll(toAdd.findTransfersWithSymbol(s));
                TransferFunction result = TransferFunction.add(base,f.getSource().toString());
                newGenerated.add(result);
                if(f.getSource().equals(initial_state)){
                    initial_state = result.getSource();
                }
                StateIDList new_final_states = new StateIDList();
                for(StateID stateID : final_states){
                    if(f.getSource().equals(stateID)){
                        new_final_states.add(result.getSource());
                        continue;
                    }
                    new_final_states.add(stateID);
                }
                final_states = new_final_states;
            }
        }
        functions.removeAll(noMoreNeeded);
        functions.addAll(newGenerated);
        retestStates();
    }

    private void retestStates() {
        StateIDList states = new StateIDList();
        for (TransferFunction f : functions){
            if(!states.contains(f.getSource())){
                states.add(f.getSource());
            }
        }
        this.states = states;
    }
}

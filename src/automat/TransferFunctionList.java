package automat;

import alphabet.Symbol;
import core.StateID;
import core.StateIDList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jleis on 04.05.2017.
 */
public class TransferFunctionList extends ArrayList<TransferFunction> {
    @Override
    public boolean add(TransferFunction function) {
        return contains(function) || super.add(function);
    }

    @Override
    public String toString() {
        List<String> result = new ArrayList<>();
        for (TransferFunction f : this) {
            result.add(f.toString());
        }
        return String.join(", ", result);
    }

    public TransferFunctionList findTransfersWithSources(StateIDList targets) {
        TransferFunctionList list = new TransferFunctionList();
        for (StateID target : targets){
            list.addAll(findTransfersWithSource(target));
        }
        return list;
    }

    public TransferFunctionList findTransfersWithSource(StateID source) {
        TransferFunctionList list = new TransferFunctionList();
        for (TransferFunction f : this){
            if(f.getSource().equals(source) && !f.getSymbol().isEpsilonSymbol()){
                list.add(f);
            }
        }
        return list;
    }



    public TransferFunctionList findEpsilonTransfers() {
        TransferFunctionList list = new TransferFunctionList();
        for(TransferFunction function : this){
            if(function.getSymbol().isEpsilonSymbol()){
                list.add(function);
            }
        }
        return list;
    }

    public TransferFunctionList findTransfersWithSymbol(Symbol s) {
        TransferFunctionList list = new TransferFunctionList();
        for(TransferFunction function : this){
            if(function.getSymbol().equals(s)){
                list.add(function);
            }
        }
        return list;
    }
}

package grammar;

import alphabet.Alphabet;
import core.StateID;

/**
 * Created by jleis on 04.05.2017.
 */
public class GrammarStateFactory {

    public static GrammarState createFromString(Alphabet alphabet, String toParse) {
        String[] first_splitting = toParse.split("->");
        GrammarState result = new GrammarState(new StateID(first_splitting[0].trim()));
        String[] possibilities = first_splitting[1].trim().split("\\|");
        for (String possibility : possibilities) {
            String trimmed = possibility.trim();
            String[] entries = trimmed.split(" ");
            Possibility for_list = new Possibility();
            for (String entry : entries) {
                PossibilityType type_of_entry = PossibilityType.SYMBOL;
                if (!alphabet.getSymbols().contains(entry)) {
                    type_of_entry = PossibilityType.STATE;
                }
                for_list.getEntries().add(new PossibilityEntry(type_of_entry, entry));
            }
            result.getPossibilities().add(for_list);
        }
        return result;
    }
}

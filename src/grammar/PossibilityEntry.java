package grammar;

/**
 * Created by jleis on 04.05.2017.
 */
public class PossibilityEntry {
    private PossibilityType type;
    private String name;

    public PossibilityEntry(PossibilityType type, String name) {
        this.type = type;
        this.name = name;
    }

    public PossibilityType getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return (getType().compareTo(PossibilityType.SYMBOL) == 0 ? "SYMBOL:" : "STATE:") + getName();
    }
}

package grammar;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by jleis on 04.05.2017.
 */
public class PossibilityEntryList extends ArrayList<PossibilityEntry> {
    public PossibilityEntryList(PossibilityEntry[] entries) {
        super(Arrays.asList(entries));
    }

    public PossibilityEntryList() {
        super();
    }
}

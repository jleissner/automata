package grammar;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jleis on 04.05.2017.
 */
public class Possibility {
    private PossibilityEntryList entries;

    public Possibility() {
        entries = new PossibilityEntryList();
    }

    public Possibility(PossibilityEntry... entries) {
        this.entries = new PossibilityEntryList(entries);
    }

    public PossibilityEntryList getEntries() {
        return entries;
    }

    @Override
    public String toString() {
        List<String> result = new ArrayList<>();
        for (PossibilityEntry e : entries) {
            result.add(e.getName());
        }
        return String.join("", result);
    }
}

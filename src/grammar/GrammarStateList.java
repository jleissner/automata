package grammar;

import core.StateID;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by jleis on 04.05.2017.
 */
public class GrammarStateList extends ArrayList<GrammarState> {

    public GrammarStateList(GrammarState... states) {
        super(Arrays.asList(states));
    }

    public GrammarState getByID(StateID id) {
        return get(indexOf(id));
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (GrammarState state : this) {
            result.append(state.getID()).append(" -> ").append(state.getPossibilities()).append(indexOf(state) != size() - 1 ? "\n" : "");
        }
        return result.toString();
    }

    public void printToConsole() {
        System.out.println(toString());
    }
}

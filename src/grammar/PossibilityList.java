package grammar;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jleis on 04.05.2017.
 */
public class PossibilityList extends ArrayList<Possibility> {
    @Override
    public String toString() {
        List<String> result = new ArrayList<>();
        for (Possibility p : this) {
            result.add(p.toString());
        }
        return String.join(" | ", result);
    }
}

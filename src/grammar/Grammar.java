package grammar;

import alphabet.Epsilon;
import core.StateID;

/**
 * Created by jleis on 04.05.2017.
 */
public class Grammar {
    private GrammarStateList states;
    private StateID initial_state;

    public Grammar(GrammarStateList states, StateID initial_state) {
        this.states = states;
        this.initial_state = initial_state;
    }

    public Grammar(GrammarStateList states) {
        this(states, states.get(0).getID());
    }

    public GrammarStateList getStates() {
        return states;
    }

    public boolean isRegular(boolean with_output) {
        boolean success = true;
        if (with_output) {
            System.out.println("----TESTING GRAMMAR FOR REGULARITY----");
        }
        loop:
        //Label for breaking!
        for (GrammarState state : states) {
            if (with_output) {
                System.out.println("Testing State '" + state + " -> " + state.getPossibilities() + "'...");
            }
            for (Possibility possibility : state.getPossibilities()) {
                int counter_states = 0;
                int counter_symbols = 0;
                for (PossibilityEntry entry : possibility.getEntries()) {
                    if (entry.getType().equals(PossibilityType.SYMBOL)) {
                        counter_symbols++;
                        if (entry.getName().equals(new Epsilon().toString())) {
                            if (with_output) {
                                System.out.println("The Grammar ist irregular because there were an Epsilon-Symbol in one State.");
                            }
                            success = false;
                            break loop;
                        }
                    } else {
                        counter_states++;
                    }
                }
                if (with_output) {
                    System.out.println("    '" + possibility + "' = Symbols: " + counter_symbols + "; States: " + counter_states);
                }
                if (counter_states > 1 || counter_symbols > 1) {
                    if (with_output) {
                        System.out.println("The Grammar ist irregular because there were more than one State or Symbol in one State.");
                    }
                    success = false;
                    break loop;
                }
            }
        }
        if (with_output) {
            System.out.println("----TESTING GRAMMAR FOR REGULARITY FINISHED----");
        }
        return success;
    }

    public boolean isRegular() {
        return isRegular(false);
    }

    @Override
    public String toString() {
        return "+ -> " + initial_state + "\n" + states;
    }
}

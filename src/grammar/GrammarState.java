package grammar;

import core.State;
import core.StateID;

/**
 * Created by jleis on 04.05.2017.
 */
public class GrammarState extends State {

    private PossibilityList possibilities;

    public GrammarState(StateID id) {
        super(id);
        possibilities = new PossibilityList();
    }

    public PossibilityList getPossibilities() {
        return possibilities;
    }
}

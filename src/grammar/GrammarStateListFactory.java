package grammar;

import alphabet.Alphabet;

/**
 * Created by jleis on 04.05.2017.
 */
public class GrammarStateListFactory {

    public static GrammarStateList createFromString(Alphabet alphabet, String toParse) {
        GrammarStateList list = new GrammarStateList();
        String[] states = toParse.split(";");
        for (String state : states) {
            list.add(GrammarStateFactory.createFromString(alphabet, state));
        }
        return list;
    }
}

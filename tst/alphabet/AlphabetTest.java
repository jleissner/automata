package alphabet;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Jan-Frederik Leißner on 04.05.2017.
 */
class AlphabetTest {
    @Test
    void addSymbolAsString() {
        Alphabet alphabet = new Alphabet();
        alphabet.addSymbol("0");
        alphabet.addSymbol("1");
        assertEquals(alphabet.toString(),"{0;1}");
    }

    @Test
    void addEpsilon() {
        Alphabet alphabet = new Alphabet();
        alphabet.addEpsilon();
        assertEquals(alphabet.toString(),"{ε}");
    }

}